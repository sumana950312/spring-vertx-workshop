package com.sumana.verticle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.sumana.service.basicoperations.BasicOperationsService;
import com.vertical.verticles.MicroServiceVerticle;

import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;

import static com.sumana.service.basicoperations.BasicOperationsService.*;

@Component
@Scope("prototype")
public class OperationsVerticle extends MicroServiceVerticle {
	
	@Autowired
	@Qualifier("BasicOperationServiceReactive")
	private BasicOperationsService basicOperationsService;
	
	@Override
	public void start(Future<Void> startFuture) throws Exception {
		Future<Void> bindServiceFuture = Future.future();
		Future<Void> serviceRegistrationFuture = Future.future();

		bindService(ADDRESS, BasicOperationsService.class, basicOperationsService, bindServiceFuture);
		publishEventBusService(EVENT_ADDRESS, ADDRESS, BasicOperationsService.class, true, true, serviceRegistrationFuture);
		
		CompositeFuture.all(bindServiceFuture, serviceRegistrationFuture).setHandler(ar -> {
			if (ar.succeeded()) {
				startFuture.complete();
			} else {
				startFuture.fail(ar.cause());
			}
		});
	}
}
//vertx.eventBus().<JsonObject>consumer("prueba").handler(message -> {
//System.out.println(message.body());
//message.reply(new JsonObject()
//		.put("result", "Todo bien perrito -> " 
//+ message.body().getInteger("result")));
//});
