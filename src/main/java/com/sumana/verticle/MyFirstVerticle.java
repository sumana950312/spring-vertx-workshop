package com.sumana.verticle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.sumana.service.basicoperations.BasicOperationsService;
import com.vertical.adapter.GenericEbAdapter;
import com.vertical.verticles.MicroServiceVerticle;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

@Component
//@Scope("prototype")
public class MyFirstVerticle extends MicroServiceVerticle {
//	@Autowired
//	BasicOperationsService basicOperationsService;
	
	@Autowired
	private GenericEbAdapter ebAdapter;
	
	@Override
	public void start(Future<Void> startFuture) throws Exception {
		Router router = Router.router(vertx);
		
//		router.handleFailure();
		
		router.get("/api").handler(ctx -> {
			JsonObject payload = new JsonObject()
					.put("numbers", new JsonArray().add(5)
							.add(12).add(100));
			
			ebAdapter.<JsonObject>discoverAddressAndSend("BasicOperations", "sumar", payload, ar -> {
				if (ar.succeeded()) {
					ctx.response().setStatusCode(200).end(ar.result().encode());
				} else {
					System.err.println(ar.cause());
				}
			});
			
		});
		
		router.get("/wait").blockingHandler(ctx -> {
			JsonObject payload = new JsonObject();
			ebAdapter.<Void>discoverAddressAndSend("BasicOperations", "esperar", payload, ar -> {
				if (ar.succeeded()) {
					ctx.response().setStatusCode(200).end("OK");
				} else {
					System.err.println(ar.cause());
				}
			});
		});
		HttpServerOptions options = new HttpServerOptions();
		options.setPort(8989);
		HttpServer httpServer = vertx.createHttpServer(options);
		httpServer.requestHandler(router::handle).listen(ar -> {
			if (ar.succeeded()) {
				startFuture.complete();
			} else {
				startFuture.fail(ar.cause());
			}
		});
	}
}

