package com.sumana;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;

import com.sumana.verticle.MyFirstVerticle;
import com.sumana.verticle.OperationsVerticle;
import com.vertical.verticles.deployment.VerticleApplication;

import io.vertx.core.Vertx;

@SpringBootApplication
@ComponentScan({ "com.vertical", "com.sumana" })
public class DemoSpringApplication extends VerticleApplication {
//	
//	@Autowired
//	private Vertx vertx;
//	
//	@Autowired
//	private MyFirstVerticle myFirstVerticle;
//	
//	@Autowired
//	private OperationsVerticle operationsVerticle;

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringApplication.class, args);
	}

	@EventListener
	private void deploy(ApplicationReadyEvent evt) {
		deployVerticle(OperationsVerticle.class.getName());
	}

}
