package com.sumana.service.basicoperations.impl;

import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.sumana.service.basicoperations.BasicOperationsService;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;

@Service
@Qualifier("BasicOperationsService")
public class BasicOperationsServiceImpl implements BasicOperationsService {
	@Autowired
	private Vertx vertx;
	
	@Override
	public void sumar(JsonObject payload, Handler<AsyncResult<JsonObject>> resultHandler) {
		vertx.executeBlocking(future -> {
			AtomicInteger result = new AtomicInteger(0);

			if (!payload.containsKey("numbers")) {
				future.fail(new ServiceException(654, "Invalid payload"));
				return;
			}

			JsonArray nums = payload.getJsonArray("numbers");
			nums.forEach(o -> {
				result.getAndAdd((Integer) o);
			});

			future.complete(new JsonObject().put("result", result.get()));

		}, false, resultHandler);
	}

	@Override
	public void restar(JsonObject payload, Handler<AsyncResult<JsonObject>> resultHandler) {
		vertx.executeBlocking(future -> {
			AtomicInteger result = new AtomicInteger(0);

			if (!payload.containsKey("numbers")) {
				future.fail(new ServiceException(654, "Invalid payload"));
				return;
			}

			JsonArray nums = payload.getJsonArray("numbers");
			nums.forEach(o -> {
				result.getAndAdd((Integer) o);
			});

			future.complete(new JsonObject().put("result", result.get()));

		}, false, resultHandler);
	}

	@Override
	public void multiplicar(JsonObject payload, Handler<AsyncResult<JsonObject>> resultHandler) {
		vertx.executeBlocking(future -> {
			AtomicInteger result = new AtomicInteger(0);

			if (!payload.containsKey("numbers")) {
				future.fail(new ServiceException(654, "Invalid payload"));
				return;
			}

			JsonArray nums = payload.getJsonArray("numbers");
			nums.forEach(o -> {
				result.getAndAdd((Integer) o);
			});

			future.complete(new JsonObject().put("result", result.get()));

		}, false, resultHandler);
	}

	@Override
	public void esperar(JsonObject payload, Handler<AsyncResult<Void>> resultHandler) {
		vertx.executeBlocking(future -> {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				future.fail(new ServiceException(4635, "Interrupted exception"));
				return;
			}
			future.complete();
		}, false, resultHandler);
	}

}
