package com.sumana.service.basicoperations.impl;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.sumana.service.basicoperations.BasicOperationsService;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.CompletableHelper;
import io.vertx.reactivex.ObservableHelper;
import io.vertx.reactivex.SingleHelper;
import io.vertx.serviceproxy.ServiceException;

@Service
@Qualifier("BasicOperationServiceReactive")
public class BasicOperationsReactiveServiceImpl implements BasicOperationsService {

	@Autowired
	private Vertx vertx;

	@Override
	public void sumar(JsonObject payload, Handler<AsyncResult<JsonObject>> resultHandler) {
		vertx.executeBlocking(future -> {
			AtomicInteger result = new AtomicInteger(0);

			if (!payload.containsKey("numbers")) {
				future.fail(new ServiceException(654, "Invalid payload"));
				return;
			}

			JsonArray nums = payload.getJsonArray("numbers");
			nums.forEach(o -> {
				result.getAndAdd((Integer) o);
			});

			future.complete(new JsonObject().put("result", result.get()));

		}, false, resultHandler);
	}

	@Override
	public void restar(JsonObject payload, Handler<AsyncResult<JsonObject>> resultHandler) {
		Single.just(payload).subscribeOn(Schedulers.newThread()).flatMap(pl -> {
			if (!pl.containsKey("numbers")) {
				return Single.error(new ServiceException(563, "Invalid payload"));
			}
			return Single.just(pl.getJsonArray("numbers"));
		}).map(ja -> {
			AtomicInteger result = new AtomicInteger(0);
			ja.forEach(i -> {
				if (result.get() == 0) {
					result.addAndGet((Integer) i);
				} else {
					result.addAndGet(-((Integer) i));
				}
			});
			return result.get();
		}).map(res -> new JsonObject().put("result", res)).subscribe(SingleHelper.toObserver(resultHandler));
	}

	@Override
	public void multiplicar(JsonObject payload, Handler<AsyncResult<JsonObject>> resultHandler) {
		Single.just(payload).subscribeOn(Schedulers.newThread()).flatMap(pl -> {
			if (!pl.containsKey("numbers")) {
				return Single.error(new ServiceException(563, "Invalid payload"));
			}
			return Single.just(pl.getJsonArray("numbers"));
		}).map(ja -> {
			AtomicInteger result = new AtomicInteger(0);
			ja.forEach(i -> {
				if (result.get() == 0) {
					result.set((Integer) i);
				} else {
					result.set(result.get() * ((Integer) i));
				}
			});
			return result.get();
		}).map(res -> new JsonObject().put("result", res)).subscribe(SingleHelper.toObserver(resultHandler));
	}

	@Override
	public void esperar(JsonObject payload, Handler<AsyncResult<Void>> resultHandler) {
		Completable.timer(10, TimeUnit.SECONDS, Schedulers.io()).subscribe(CompletableHelper.toObserver(resultHandler));
	}
}
