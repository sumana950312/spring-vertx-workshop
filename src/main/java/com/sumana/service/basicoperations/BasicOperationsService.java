package com.sumana.service.basicoperations;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

@ProxyGen
public interface BasicOperationsService {
	String EVENT_ADDRESS = "BasicOperationsService";
	String ADDRESS = "BasicOperations";
	public void sumar(JsonObject payload, Handler<AsyncResult<JsonObject>> resultHandler);
	public void restar(JsonObject payload, Handler<AsyncResult<JsonObject>> resultHandler);
	public void multiplicar(JsonObject payload, Handler<AsyncResult<JsonObject>> resultHandler);
	public void esperar(JsonObject payload, Handler<AsyncResult<Void>> resultHandler);
}
